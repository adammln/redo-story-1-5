#STORY1 - APP URLS#
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from .views import index

urlpatterns = [
	path('', index, name='story1_index'),
]
