from .models import JadwalPribadi
from django import forms

fieldStyling = (
	'margin-left: auto;'
	+'margin-right: auto;'
	+'margin-top: 10px;'
	+'margin-bottom: 10px;'
	+'padding-left: 25px;'
	+'padding-right: 25px;'
	+'padding-top: 5px;'
	+'padding-top: 5px;'
	+'min-width: 260px;'
	+'justify-content: center;'
)

class JadwalForm(forms.Form):
	date = forms.DateField(label='',
		required=True, 
		#input_formats=settings.DATE_INPUT_FORMATS,
		widget=forms.DateInput(
			attrs={
			'class' : 'input-group-text',
			'id' : 'f_date',
			'type':'date',
			'style' : fieldStyling,
			}
		)
	)
	time = forms.TimeField(label='',
		required=True,
		widget=forms.TimeInput(attrs={'class' : 'input-group-text','id' : 'f_time', 'type' : 'time', 'style':fieldStyling}))
	eventName = forms.CharField(label='',
		required=True,
		max_length=32,
		widget=forms.TextInput(attrs={'class' : 'form-control','id' : 'str_name', 'type':'text','placeholder':'Nama kegiatan', 'style':fieldStyling}))
	location = forms.CharField(label='',
		max_length=48,
		required=False,
		widget=forms.TextInput(attrs={'class' : 'form-control','id' : 'str_loc', 'type':'text','placeholder':'Tempat', 'style':fieldStyling}))
	category = forms.CharField(label='',
		max_length=16,
		required=False,
		widget=forms.TextInput(attrs={'class' : 'form-control','id' : 'str_ctg', 'type':'text','placeholder':'Kategori', 'style':fieldStyling}))