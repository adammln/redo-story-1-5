#STORY1 - APP URLS#
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from .views import index, clear_sched, create_sched

urlpatterns = [
	path('', index, name='story4_index'),
	path('story5-clr-event/', clear_sched, name='clr_event'),
	path('story5-crt-event/', create_sched, name='crt_event'),
]
