from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import JadwalPribadi
from .forms import JadwalForm

response = {}
# def index(request):
# 	return render(request, 'index4.html', response)

def index(request):
	response['schedules'] = JadwalPribadi.objects.all()
	return render(request, 'index4.html', response)

def clear_sched(request):
	JadwalPribadi.objects.all().delete()
	return HttpResponseRedirect('/story4')

def create_sched(request):
	form = JadwalForm(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['date'] = request.POST.get('date')	
		response['time'] = request.POST.get('time')
		response['event_name'] = request.POST.get('event_name')
		response['location'] = request.POST.get('location')
		response['category'] = request.POST.get('category')


		Jadwal_submit = JadwalPribadi(
			date=response['date'],
			time=response['time'],
			event_name=response['event_name'],
			location=response['location'], 
			category=response['category'],
		)

		Jadwal_submit.save()
		return HttpResponseRedirect('/story4')
	else :
		response['form_sched'] = JadwalForm()
	return render(request, 'sched.html', response)
